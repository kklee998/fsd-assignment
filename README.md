# README #

FSD Assignment CT010-3-1

### What is this repository for? ###

Completing FSD coding assignment

### How do I get set up? ###

Install git
Install python

### Contribution guidelines ###

1.) Clone the repo with git clone

2.)Create your own branch to push the code into (git branch <Your name here> && git checkout <Your name Here>

3.)Once you have finished editing the code, use git push origin <your branch name here>

### Who do I talk to? ###

Lee Kah Kin