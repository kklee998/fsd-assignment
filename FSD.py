import sys
import time
import datetime
import string
import re
#jonah tries a git command
def main_menu():

	print("===============================================================================")
	print("*				 Main Menu                        	      *")
	print("===============================================================================")	

	print("				(R)egister")
	print("				(S)tatus")
	print("				(C)heckout")
	print("				(L)ogout")
	print("				(E)xit")	  

def apartment_info():

	print("***********************************************************************************************************************************")
	
	print("Apartment Type A:\n2 bedrooms and equiped with kitchen and laundry facilities.\n"
		  "The monthly rental for the rooms in this apartment type is RM300. \n")
	
	print("Apartment Type B:\n3 bedrooms includes one master bedroom with attached bathroom" 
		  "but does not have kitchen and laundry facilities."
		  
		  "\nThe monthly rental for the rooms in this apartment type is RM200."
		  "\nStudents staying in the master bedroom will be paying an additional 40%.")
	
	print("***********************************************************************************************************************************")

def create_blank_file():
	#checks if text file exist, else creates blank
	blank_file_A = "type_a_apartment_list"
	try:
		open(blank_file_A,'x')
	except FileExistsError:
		pass
	blank_file_B = "type_b_apartment_list"
	try:
		open(blank_file_B,'x')
	except FileExistsError:
		pass	

def ID_check():

	print("********************************************************************************")
	print("*			   University Apartment System                         *")
	print("*			   Today's date is:",datetime.date.today(),"                        *")
	print("********************************************************************************")

	#validating student credentials
	student_ID = input("\nPlease enter your TP number(exactly 6 digits): \n")
	if str.isdigit(student_ID) and len(student_ID) == 6 :
		student_ID = 'TP'+student_ID
		print("Your TP number is:",student_ID)
	else:
		print("Invalid input, please only write EXACTLY 6 numbers and 6 numbers only.")
		sys.exit()

	return student_ID	

def duplication_check():

	Dcheck_bool = False
	#checks the files for existing ID number
	Dcheck_A = open('type_a_apartment_list','r')
	Dcheck_B = open('type_b_apartment_list','r')

	for line in Dcheck_A:
		line = line.rstrip(',')
		if student_ID in line:
			Dcheck_bool = True

	for line in Dcheck_B:
		line = line.rstrip(',')
		if student_ID in line:
			Dcheck_bool = True		

	return Dcheck_bool

def student_name_validation():

	while True:
		student_name = input("Please enter your name: \n")
		student_name_check = ''.join(student_name.split())
		#removes whitespace
		if str.isalpha(student_name_check) == False:
			#checks for non-alphabet input
			print("Invalid input, please do not include number and/or symbols.")
		else:
			print("Your name is:",student_name)
			break

	return student_name

def type_A_confirmation_check():

	print("********************************************************************************")
	print("These are the details of your registration:")
	print("********************************************************************************")
	print("================================================================================")
	print("The unit you will live in is,",A_unit)
	print("================================================================================")
	print("Utility Charges:         ", utility_charges)
	print("One month rental:         300")
	print("One month rental deposit: 300")
	print("-------------------------------")
	print("Total 			 :700")
	print("\nThis is your checkout date:",checkout_day)

	while True:
		type_A_prompt = input("Do you wish to proceed (y/n):")

		if type_A_prompt.lower() == 'y':
			type_A_confirmation = True
			break
			
		elif type_A_prompt.lower() == 'n':
			type_A_confirmation = False
			break
		else:
			print("Invalid input, please only select y or n.")
			

	return type_A_confirmation

def type_B_confirmation_check():

	if room_selection == 'master':
		room_B_rent = 280

	else:
		room_B_rent = 200

	print("********************************************************************************")
	print("Before you proceed, these are the payments you must make:")
	print("********************************************************************************")
	print("================================================================================")
	print("The unit you will live in is,",B_unit)
	print("================================================================================")
	print("Utility Charges:         ", utility_charges)
	print("One month rental:        ", room_B_rent)
	print("One month rental deposit:",room_B_rent)
	print("-------------------------------")
	print("Total 			:",utility_charges+room_B_rent+room_B_rent)
	print("\nThis is your checkout date:",checkout_day)


	type_B_prompt = input("Do you wish to proceed (y/n):")
	while True:

		if type_B_prompt.lower() == 'y':
			type_B_confirmation = True
			break
			
		elif type_B_prompt.lower() == 'n':
			type_B_confirmation = False
			break
		else:
			print("Invalid input, please only select y or n.")

	return type_B_confirmation		

########################################################################################################
current_day = datetime.date.today()
checkout_day = str(current_day+datetime.timedelta(140))
utility_charges = 100
create_blank_file()

while True:

	student_ID = ID_check()

	while True:

		main_menu()
		menu_button = input("Please select an option: ")
		if menu_button.lower() == "r":
					#Registering new students into an apartment
			Dcheck_bool = duplication_check()				
			if Dcheck_bool == False:
				student_name = student_name_validation()
				apartment_info()
				apartment_selection = input("\nPlease select an apartment to register to (A/B): \n")
				#Select a room from the available apartment.

				if apartment_selection.lower() == "a":

					while True:
						'''
						c

						'''
						A_unit_test = []
						A_occupant_check = []
						b = 0
						total_occupant = 0
						Ochecker = 0
						Ocheck = open('type_a_apartment_list','r')
						A_unit_bool = False

						#checks for number of occupants
						for Ocheck_line in Ocheck:
							Ocheck_line = Ocheck_line.split(',') #creates a list from the file
							Ochecker = Ocheck_line[2] #index[2] contains apartment unit number
							
							A_unit_test.append(int((re.search('\d+',Ochecker).group()))) #splits the number from 'A1' into '1'
							
							Ochecker = int(re.search('\d+',Ochecker).group())

							total_occupant = total_occupant + Ochecker
					
						Ocheck.close()
						
						while True:
						
							A_unit = int(input("Please select a unit number."))
							for b in A_unit_test:
								if b == A_unit:
									A_occupant_check.append(b) #counts number of tennants in 1 unit
									
							if len(A_occupant_check) >= 3:

								print("This unit is full, please try again.")
								break
							else:
								A_unit_bool = True #unit contains less than 3 tennants
								break

						if A_unit_bool == True:

							type_A_confirmation = type_A_confirmation_check()

							if type_A_confirmation == True:

								#information recorded in text files
								type_A_register = open('type_a_apartment_list', 'a')

								type_A_register.write(student_ID+',')
								type_A_register.write(student_name+',')
								type_A_register.write('A'+str(A_unit)+',')
								type_A_register.write(checkout_day+'\n')
								type_A_register.close()

								print("Your information has been saved successfully.")
								break

							else:
								print("\nYou may not register for this apartment unless you agree to the terms and conditions.\n")
								break	

				elif apartment_selection.lower() == "b":

					while True:
						'''
						Checks for number of occupants in the apartment_B

						'''
						B_unit_test = []
						B_occupant_check = []
						c = 0
						master_room_test = []
						total_occupant = 0
						Ochecker = 0
						Ocheck = open('type_b_apartment_list','r')
						Rcheck = open('type_b_apartment_list','r')
						B_unit_bool = False
						master_room_full = False
						normal_room_full = False

						#check for number of occupants
						for Ocheck_line in Ocheck:
							
							Ocheck_line = Ocheck_line.split(',')
							Ochecker = Ocheck_line[2]
							
							B_unit_test.append(int((re.search('\d+',Ochecker).group())))
							Ochecker = int(re.search('\d+',Ochecker).group())

							total_occupant = total_occupant + Ochecker

						while True:

							B_unit = int(input("Please select a unit number."))
							B_unit_bool = False
							for c in B_unit_test:
								if c == B_unit:
									B_occupant_check.append(c)

							if len(B_occupant_check) >= 3:

								print("This unit is full, please try again.")
								break
							else:
								B_unit_bool = True

							room_selection = input("(M)aster room OR (N)ormal room?\n")

							#checks master room availability
							for Rcheck_line in Rcheck:
								
								Rcheck_line = Rcheck_line.split(',') #creates a list from the string
								Rchecker = Rcheck_line[3] #index[3] is information on room types
								
								master_room_test.append(Rchecker)
								
								if 'master' in Rchecker:
									if len(B_occupant_check) % 3 != 0:
										master_room_full = True
									else:
										master_room_full = False

							if room_selection.lower() == 'm':

								if master_room_full == False:
									room_selection = 'master'
									break
									
								else:
									print("The master room for this unit is taken.")

							elif room_selection.lower() == 'n':

									room_selection = 'normal'
									break

							else:
								print("Please only enter M or N.")

							
						if B_unit_bool == True:

							type_B_confirmation = type_B_confirmation_check()
							if type_B_confirmation == True:

								#record information into text

								type_B_register = open('type_b_apartment_list', 'a')
								type_B_register.write(student_ID+',')
								type_B_register.write(student_name+',')
								type_B_register.write('B'+str(B_unit)+',')
								type_B_register.write(room_selection+',')
								type_B_register.write(checkout_day+'\n')
								type_B_register.close()

								print("Your information has been saved successfully.")
								break
							else:
								print("\nYou may not register for this apartment unless you agree to the terms and conditions.\n")
								break
				else:
					print("\nPlease enter a valid option")
			else:
				print("You have already registered for an apartment here.")	

		
		elif menu_button.lower() == "s":

			#Check status of the student ID

			status = False
			
			status_check_A = open('type_a_apartment_list','r')
			for status_line in status_check_A:
				status_line = status_line.rstrip(',')
				if student_ID in status_line:
						print(status_line)
						status = True
			status_check_B = open('type_b_apartment_list','r')
			for status_line in status_check_B:
				status_line = status_line.rstrip(',')
				if student_ID in status_line:
						print(status_line)
						status = True
						break

			if status == False:
				print("You have not registered for an apartment here.")							

		elif menu_button.lower() == "c":

			#removes Student from the text file

			checkout_confirmation = input("Are you sure you wish to checkout? (Y/N)")
			
			if checkout_confirmation.lower() == 'y':

				checkout_file_A = open("type_a_apartment_list","r+")
				checkout_line_A = checkout_file_A.readlines()
				checkout_file_A.seek(0)
				for new_checkout_line_A in checkout_line_A:
					if student_ID not in new_checkout_line_A:
						checkout_file_A.write(new_checkout_line_A)
				
				checkout_file_A.truncate()
				checkout_file_A.close()

				checkout_file_B = open("type_b_apartment_list","r+")
				checkout_line_B = checkout_file_B.readlines()
				checkout_file_B.seek(0)
				for new_checkout_line_B in checkout_line_B:
					if student_ID not in new_checkout_line_B:
						checkout_file_B.write(new_checkout_line_B)
				
				checkout_file_B.truncate()
				checkout_file_B.close()
				print("You have been successfully checked out.")

			else:
				print("You will not be checking out today.")	

		elif menu_button.lower() == "e":
			sys.exit()

		elif menu_button.lower() == "l":
			#returns to before main_menu()
			break

		else:
			print("\nPlease enter a valid option")	
